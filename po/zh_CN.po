# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
#
# Translators:
# biqiu-ssw <shenshuaiwen@gmail.com>, 2018
# Careone <zzbusagain@yahoo.com.cn>, 2009
# liushuyu011 <liushuyu011@gmail.com>, 2016-2018
# Mingcong Bai <jeffbai@aosc.xyz>, 2017-2018
# Mingye Wang <arthur200126@gmail.com>, 2016
# zhangxianwei8 <zhang.xianwei8@zte.com.cn>, 2018
# 玉堂白鹤 <yjwork@qq.com>, 2015,2017
# Mingcong Bai <jeffbai@aosc.xyz>, 2016
# Mingcong Bai <jeffbai@aosc.xyz>, 2015
msgid ""
msgstr ""
"Project-Id-Version: MATE Desktop Environment\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-31 12:11+0100\n"
"PO-Revision-Date: 2018-12-26 03:12+0000\n"
"Last-Translator: Mingcong Bai <jeffbai@aosc.xyz>\n"
"Language-Team: Chinese (China) (http://www.transifex.com/mate/MATE/language/"
"zh_CN/)\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#: ../mate-tweak:1203
msgid "Right"
msgstr "右"

#: ../mate-tweak:1204
msgid "Left"
msgstr "左"

#: ../mate-tweak:1212
msgid "Marco (No compositor)"
msgstr "Marco（无混成器）"

#: ../mate-tweak:1214
msgid "Marco (Adaptive compositor)"
msgstr "Marco（自适应混成器）"

#: ../mate-tweak:1216
msgid "Marco (Compton GPU compositor)"
msgstr "Marco（Compton GPU 混成器）"

#: ../mate-tweak:1218
msgid "Compiz (Advanced GPU accelerated desktop effects)"
msgstr "Compiz（高级GPU加速的桌面效果）"

#: ../mate-tweak:1221
msgid ""
"You are currently using an unknown and unsupported window manager. Thus we "
"cannot guarantee that changes made here will be effective."
msgstr ""
"你现在正在使用一个未知的且不受支持的窗口管理器。因此我们不能保证你在这里作出"
"的修改可以生效。"

#: ../mate-tweak:1523
msgid "Desktop"
msgstr "桌面"

#: ../mate-tweak:1524
msgid "Interface"
msgstr "界面"

#: ../mate-tweak:1525
msgid "Panel"
msgstr "面板"

#: ../mate-tweak:1526
msgid "Windows"
msgstr "窗口"

#: ../mate-tweak:1575
msgid "MATE Tweak"
msgstr "MATE Tweak"

#: ../mate-tweak:1578
msgid "Desktop icons"
msgstr "桌面图标"

#: ../mate-tweak:1579
msgid "Performance"
msgstr "性能"

#: ../mate-tweak:1580
msgid "Window Behaviour"
msgstr "窗口行为"

#: ../mate-tweak:1581
msgid "Appearance"
msgstr "外观"

#: ../mate-tweak:1582
msgid "Panels"
msgstr "面板"

#: ../mate-tweak:1583
msgid "Panel Features"
msgstr "面板特性"

#: ../mate-tweak:1584
msgid "Panel Menu Features"
msgstr "面板菜单特性"

#: ../mate-tweak:1585
msgid "Icons"
msgstr "图标"

#: ../mate-tweak:1586
msgid "Context menus"
msgstr "上下文菜单"

#: ../mate-tweak:1587
msgid "Toolbars"
msgstr "工具栏"

#: ../mate-tweak:1588
msgid "Window manager"
msgstr "窗口管理器"

#: ../mate-tweak:1590
msgid "Select the Desktop Icons you want enabled:"
msgstr "选择您想在桌面上启用的图标："

#: ../mate-tweak:1591
msgid "Show Desktop Icons"
msgstr "显示桌面图标"

#: ../mate-tweak:1592
msgid ""
"When disabled the desktop will be unmanaged with no icons or file manager "
"access"
msgstr "禁用后桌面上将不显示图标或提供文件管理功能"

#: ../mate-tweak:1593
msgid "Computer"
msgstr "计算机"

#: ../mate-tweak:1594
msgid "Home"
msgstr "主目录"

#: ../mate-tweak:1595
msgid "Network"
msgstr "网络"

#: ../mate-tweak:1596
msgid "Trash"
msgstr "回收站"

#: ../mate-tweak:1597
msgid "Mounted Volumes"
msgstr "已挂载的卷"

#: ../mate-tweak:1599
msgid "Enable animations"
msgstr "启用动画"

#: ../mate-tweak:1600
msgid "Whether animations should be displayed by the window manager and panel"
msgstr "是否使用窗口管理器及面板显示动画"

#: ../mate-tweak:1601
msgid "Do not show window content when moving windows"
msgstr "移动窗口时不显示窗口内容"

#: ../mate-tweak:1602
msgid "Provide less feedback when moving windows by using wireframes"
msgstr "移动窗口时仅显示边框"

#: ../mate-tweak:1603
msgid "Window manager performance tuning."
msgstr "窗口管理器性能微调。"

#: ../mate-tweak:1605
msgid "Enable window snapping"
msgstr "启用窗口吸附"

#: ../mate-tweak:1606
msgid ""
"Dropping windows on screen edges maximizes them vertically and resizes them "
"horizontally to cover half the available area"
msgstr "将窗口放置到屏幕边缘将垂直或水平最大化窗口为屏幕面积的一半"

#: ../mate-tweak:1607
msgid "Undecorate maximized windows"
msgstr "隐藏最大化窗口的装饰"

#: ../mate-tweak:1608
msgid "Undecorate windows when maximized"
msgstr "最大化时隐藏窗口装饰"

#: ../mate-tweak:1609
msgid "Do not auto-maximize new windows"
msgstr "禁用新窗口自动最大化"

#: ../mate-tweak:1610
msgid "Do not automatically maximize newly opened windows"
msgstr "禁用新窗口自动最大化"

#: ../mate-tweak:1611
msgid "Window control placement."
msgstr "窗口控制的摆放位置。"

#: ../mate-tweak:1613
msgid "Save the current panel layout as your own custom version"
msgstr "保存当前面板布局为自定义配置"

#: ../mate-tweak:1614
msgid "Delete the currently selected panel layout"
msgstr "删除当前选择的面板布局"

#: ../mate-tweak:1615
msgid "Open CCSM"
msgstr "打开 CCSM"

#: ../mate-tweak:1616
msgid "Open the Compiz configuration and settings manager"
msgstr "打开 Compiz 配置及设置管理器"

#: ../mate-tweak:1617
msgid "Reset Compiz"
msgstr "重置 Compiz"

#: ../mate-tweak:1618
msgid "Reset the current Compiz configuration to factory defaults"
msgstr "重置当前 Compiz 配置为默认值"

#: ../mate-tweak:1621 ../mate-tweak:1626
msgid "HiDPI"
msgstr "HiDPI"

#: ../mate-tweak:1622
msgid "Select a window scaling factor."
msgstr "选择一个窗口缩放因子。"

#: ../mate-tweak:1624
msgid "Auto-detect"
msgstr "自动检测"

#: ../mate-tweak:1625
msgid "Regular"
msgstr "常规"

#: ../mate-tweak:1629
msgid ""
"Double the size of all windows, panels, widgets, fonts, etc. for HiDPI "
"displays."
msgstr "设置所有窗口、面板、控件、字体等为双倍尺寸，用于HiDPI显示。"

#: ../mate-tweak:1635
msgid "Open Font preferences"
msgstr "打开字体首选项…"

#: ../mate-tweak:1636 ../mate-tweak:1638
msgid "Fonts"
msgstr "字体"

#: ../mate-tweak:1637
msgid "Open Font preferences and click Details... to fine tune the font DPI."
msgstr "打开字体首选项并单击“详细信息…”以微调字体DPI。"

#: ../mate-tweak:1640
msgid "Enable keyboard LED"
msgstr "启用键盘 LED"

#: ../mate-tweak:1641
msgid "Show keyboard LED indicators in the notifcation tray"
msgstr "在通知区域显示键盘 LED 指示灯"

#: ../mate-tweak:1642
msgid "Enable pull-down terminal"
msgstr "启用下拉终端"

#: ../mate-tweak:1643
msgid "When enabled press F12 to pull down terminal"
msgstr "启用后，按 F12 以打开下拉终端"

#: ../mate-tweak:1644
msgid "Enable Dock"
msgstr "启用 Dock"

#: ../mate-tweak:1645
msgid "When checked the Dock will be enabled."
msgstr "选中时启用 Dock"

#: ../mate-tweak:1646
msgid "Enable HUD"
msgstr "启用 HUD"

#: ../mate-tweak:1647
msgid ""
"When checked the Heads-Up Display (HUD) will be enabled. Press Alt_L to "
"search application menus."
msgstr "选中此项后将启用“抬头显示” (HUD) 。按 Alt_L 以搜索应用程序菜单。"

#: ../mate-tweak:1649
msgid "Show Applications"
msgstr "显示应用程序"

#: ../mate-tweak:1650
msgid "Show Applications item in the menu bar"
msgstr "显示应用程序菜单"

#: ../mate-tweak:1651
msgid "Show Places"
msgstr "显示位置"

#: ../mate-tweak:1652
msgid "Show Places item in the menu bar"
msgstr "显示位置菜单"

#: ../mate-tweak:1653
msgid "Show System"
msgstr "显示系统"

#: ../mate-tweak:1654
msgid "Show System item in the menu bar"
msgstr "显示系统菜单"

#: ../mate-tweak:1656
msgid "Show icons on menus"
msgstr "在菜单中显示图标"

#: ../mate-tweak:1657
msgid "Whether menus may display an icon next to a menu entry"
msgstr "是否在菜单项旁边显示图标"

#: ../mate-tweak:1658
msgid "Show icons on buttons"
msgstr "在按钮中显示图标"

#: ../mate-tweak:1659
msgid "Whether buttons may display an icon in addition to the button text"
msgstr "是否在按钮文本旁边显示图标"

#: ../mate-tweak:1660
msgid "Show Input Methods menu in context menus"
msgstr "在上下文菜单中显示输入法菜单"

#: ../mate-tweak:1661
msgid ""
"Whether the context menus of entries and text views should offer to change "
"the input method"
msgstr "是否在文本视图的右键菜单中显示切换输入法的选项"

#: ../mate-tweak:1662
msgid "Show Unicode Control Character menu in context menus"
msgstr "在上下文菜单中显示 Unicode 控制字符菜单"

#: ../mate-tweak:1663
msgid ""
"Whether the context menu of entries and text views should offer the insert "
"control characters"
msgstr "是否在文本视图的右键菜单中显示插入控制符号的选项"

#: ../mate-tweak:1665
msgid "Style:"
msgstr "样式："

#: ../mate-tweak:1666
msgid "Icon size:"
msgstr "图标尺寸："

#: ../mate-tweak:1685
msgid "Small"
msgstr "小"

#: ../mate-tweak:1686
msgid "Large"
msgstr "大"

#: ../mate-tweak:1695
msgid "The new window manager will be activated upon selection."
msgstr "在选择时激活新的窗口管理器。"

#: ../mate-tweak:1696
msgid "Select a window manager."
msgstr "选择一个窗口管理器。"

#: ../mate-tweak:1702
msgid "Select a panel layout to change the user interface."
msgstr "选择一个面板布局以改变用户界面。"

#: ../mate-tweak:1703
msgid "Select a panel layout."
msgstr "选择面板布局"

#: ../mate-tweak:1708
msgid "Default"
msgstr "默认"

#: ../mate-tweak:1709
msgid "16px"
msgstr "16px"

#: ../mate-tweak:1710
msgid "22px"
msgstr "22px"

#: ../mate-tweak:1711
msgid "24px"
msgstr "24px"

#: ../mate-tweak:1712
msgid "32px"
msgstr "32px"

#: ../mate-tweak:1713
msgid "48px"
msgstr "48px"

#: ../mate-tweak:1717
msgid "Set the panel icon size."
msgstr "设置面板图标大小"

#: ../mate-tweak:1718
msgid "Select the icon size for panel icons."
msgstr "选择面板图标的大小"

#: ../mate-tweak:1722
msgid "Set the icon size of menu items used in the panel."
msgstr "设置在面板中菜单项目图标的大小"

#: ../mate-tweak:1723
msgid "Select the icon size for menu items in the panel."
msgstr "选择面板中菜单项目图标的大小"

#: ../mate-tweak:1751
msgid "Text below items"
msgstr "文本在下面"

#: ../mate-tweak:1752
msgid "Text beside items"
msgstr "文本在旁边"

#: ../mate-tweak:1753
msgid "Icons only"
msgstr "仅图标"

#: ../mate-tweak:1754
msgid "Text only"
msgstr "仅文本"

#~ msgid "Contemporary"
#~ msgstr "现代"

#~ msgid "Cupertino"
#~ msgstr "Cupertino"

#~ msgid "Familiar"
#~ msgstr "友好"

#~ msgid "Fedora"
#~ msgstr "Fedora"

#~ msgid "GNOME2"
#~ msgstr "GNOME2"

#~ msgid "Linux Mint"
#~ msgstr "Linux Mint"

#~ msgid "Mageia"
#~ msgstr "Mageia"

#~ msgid "Manjaro"
#~ msgstr "Manjaro"

#~ msgid "Mutiny"
#~ msgstr "Mutiny"

#~ msgid "Netbook"
#~ msgstr "上网本"

#~ msgid "openSUSE"
#~ msgstr "openSUSE"

#~ msgid "Pantheon"
#~ msgstr "Pantheon"

#~ msgid "Redmond"
#~ msgstr "Redmond"

#~ msgid "Solus"
#~ msgstr "Solus"

#~ msgid "Traditional"
#~ msgstr "传统"
